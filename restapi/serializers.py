from restapi.models import Customer, Partner , Service,ServiceDateTime,Booking
from rest_framework import serializers
from restapi.utils import mobileNoValidator,setTime
from django.db.models import Max
import datetime,json
from django.http import JsonResponse
class CustomerSerializer(serializers.ModelSerializer):
	class Meta:
		model = Customer
		exclude = ('created_at','updated_at')

class ServiceSerializer(serializers.ModelSerializer):
	dateTime = serializers.SerializerMethodField('date_time_method')
	fees = serializers.IntegerField()
	discountFees = serializers.IntegerField()
	def date_time_method(self, service):
		queryset = ServiceDateTime.objects.filter(service__id=service.id)
		data =[]
		for obj in queryset:
				data.append(setTime(obj.startTime,obj.endTime))
		return data
	class Meta:
		model = Service
		fields = ('id','serviceName','fees','discountPercentage','dateTime','discountFees','runningStatus','serviceType')        

# class ServiceDateTimeSerializer(serializers.ModelSerializer):
# 	class Meta:
# 		model = ServiceDateTime
# 		fields = (,'date','startTime','endTime')             

class PartnerSerializer(serializers.ModelSerializer):
	class Meta:
		model = Partner
		fields = ('id','businessName','tagLine','partnerImgUrl','mobileNo')

class ShowBookingSerializer(serializers.ModelSerializer):
	partner_name = serializers.SerializerMethodField('partner_name_method')
	def partner_name_method(self, booking):
		return booking.partner.businessName
	class Meta:
		model = Booking
		fields = ('partner_name','created_at','booking_id','total_amount','serviceTime',"service")		

class ShowBookingPartnerSerializer(serializers.ModelSerializer):
	partner_name = serializers.SerializerMethodField('partner_name_method')
	def partner_name_method(self, booking):
		return booking.partner.businessName
	class Meta:
		model = Booking
		fields = ('partner_name','created_at','booking_id','total_amount','serviceTime','completed','id')		


class PartnerListSerializer(serializers.ModelSerializer):
	max_discount = serializers.SerializerMethodField('max_discount_method')
	dateTime = serializers.SerializerMethodField('date_time_method')
	def max_discount_method(self, partner):
		date = self.context.get("date")
		dt = datetime.datetime.strptime(date, "%Y-%m-%d")
		return partner.services.filter(servicedatetime__date=dt).aggregate(Max('discountPercentage'))['discountPercentage__max']
	def date_time_method(self, partner):
		response = "Happy Hours: "
		date = self.context.get("date")
		dt = datetime.datetime.strptime(date, "%Y-%m-%d")
		queryset = ServiceDateTime.objects.filter(service__partner__id=partner.id,date = dt)
		if(len(queryset) == 0):
			response = response + "Not Yet Check Again"
		else :
			for obj in queryset:
				response = response + setTime(obj.startTime,obj.endTime)+" "
		return response		
	class Meta:
		model = Partner
		fields = ('id','businessName','tagLine','partnerImgUrl','serviceType','max_discount','dateTime')





class ServicePartnerSerializer(serializers.ModelSerializer):
	class Meta:
		model = Service
		fields = ('id','serviceName','fees','discountPercentage','discountFees','runningStatus','serviceType')        

class ServiceDateTimePartnerSerializer(serializers.ModelSerializer):
	class Meta:
		model = ServiceDateTime
		fields = ('date', 'endTime', 'id', 'startTime','service')        

