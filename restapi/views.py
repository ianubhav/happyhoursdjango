from rest_framework import viewsets, generics
from restapi.serializers import *
from restapi.models import Customer,OtpTable,Partner ,CustomerToken,Booking,PartnerToken
from rest_framework.decorators import api_view
from django.http import JsonResponse
from restapi.utils import mobileNoStringValidator
from rest_framework.response import Response
from uuid import UUID
from django.contrib.auth.models import User
from django.db.models import Max,Q
import dateutil.parser
from geopy.distance import vincenty

@api_view(['POST'])
def PartnerList(request):
    response = { 'is_error' : True, 'message' : 'Some Message','data': [],'type':'','date':'2016-01-02'}
    serviceType = request.data.get('type')
    date = request.data.get('date')
    latitude = request.data.get('lat')
    longitude = request.data.get('lng')
    queryset = Partner.objects.filter(serviceType=serviceType)
    queryset = sorted(queryset, key= lambda t: t.distance(latitude,longitude))
    for partner in queryset:
        data = PartnerListSerializer(partner,context={'date': date})
        if(vincenty((latitude,longitude),(partner.locationLat,partner.locationLng)).km<50):
            response['data'].append(data.data)
    response['type'] = serviceType
    response['date'] = date
    response['is_error'] = False
    response['message'] = 'Successful'
    return Response(response)

@api_view(['POST'])
def PartnerHomePage(request):
    # print request.data
    response = { 'is_error' : True, 'message' : 'Some Message','data': {},'services': {}}
    try:
        partner = Partner.objects.get(id=request.data.get('id'))
        serializer = PartnerSerializer(partner)
        date = request.data.get('date')
        queryset = partner.services.filter(Q(servicedatetime__date=date)  | Q(serviceType="other")).distinct()
        serializer2 = ServiceSerializer(queryset, many=True)
        response['data'] = serializer.data
        response['services'] = serializer2.data
        response['is_error'] = False
        response['message'] = 'Successful'
        return JsonResponse(response)
    except Partner.DoesNotExist:
        response['message'] = 'Partner not found'
        return JsonResponse(response)
    # except: 
    #     response['message'] = 'Internal Server Error. Try Again'
    #     return JsonResponse(response)

@api_view(['POST'])
def verifyOtp(request):
    response = { 'is_error' : True, 'is_otp_verified' : False ,'data' : {},'message' : 'Missing Parameters','is_first_time':False}
    mobileNo = request.data.get('mobile_no')
    token = request.data.get('token')
    otp = request.data.get('otp')

    if (mobileNo and token and otp):
        if(not mobileNoStringValidator(mobileNo)):
            response['message'] = 'Please Enter Correct Mobile No'
            return JsonResponse(response)
        try:
            val = int(otp)
        except ValueError:
            response['message'] = 'Otp must be a valid 4 digit number'
            return JsonResponse(response)  
        try:
            otpObject = OtpTable.objects.get(mobileNo=mobileNo,otp=otp,token=token)
            if(otpObject):
                customer,state = Customer.objects.get_or_create(mobileNo=mobileNo)
                token,state = CustomerToken.objects.get_or_create(customer=customer)
                response['is_error'] = False
                response['is_otp_verified'] = True
                response['message'] = 'Successful'
                response['token'] = token.key
                response['is_first_time'] = state
                serializer = CustomerSerializer(customer)
                response['data'] = serializer.data
                return JsonResponse(response)  
        except OtpTable.DoesNotExist:
            response['is_error'] = False
            response['message'] = 'Incorrect otp. Please try again'
            return JsonResponse(response)
        except: 
            response['message'] = 'Internal Server Error. Try Again'
            return JsonResponse(response)      
        
        response['message'] = 'Valid Params'
        return JsonResponse(response)   
    else:
        return Response(response)    
    
@api_view(['POST'])
def updateUser(request):
    response = { 'is_error' : True, 'message' : 'Missing Parameters'}
    params = {}
    token = request.data.get('token')
    # print request.data
    if (token):
        try:
            tokenObject = CustomerToken.objects.get(key=token)
            if(request.data.get('os_version')):
                params['osVersion'] = request.data.get('os_version')
            if(request.data.get('os_type')):
                params['osType'] = request.data.get('os_type')
            if(request.data.get('device_id')):
                params['deviceId'] = request.data.get('device_id')
            if(request.data.get('default_lat')):
                params['signupLat'] = request.data.get('default_lat')
            if(request.data.get('default_lng')):
                params['signupLng'] = request.data.get('default_lng')
            if(request.data.get('current_lat')):
                params['currentLat'] = request.data.get('current_lat')
            if(request.data.get('current_lng')):
                params['currentLng'] = request.data.get('current_lng')    
            if(request.data.get('first_name')):
                params['firstName'] = request.data.get('first_name')
            if(request.data.get('last_name')):
                params['lastName'] = request.data.get('last_name')
            Customer.objects.filter(mobileNo=tokenObject.customer.mobileNo).update(**params)
            response['is_error'] = False
            response['message'] = 'Successfully Updated'
            return JsonResponse(response)  
        except CustomerToken.DoesNotExist:
            print response
            response['message'] = 'CustomerToken not found'
            return JsonResponse(response)
        # except: 
        #     response['message'] = 'Internal Server Error. Try Again'
        #     return JsonResponse(response)     
        # except: 
        #     response['message'] = 'Internal Server Error. Try Again'
        return JsonResponse(response)
    else:
        return Response(response)        

@api_view(['POST'])
def logoutUser(request):
    response = { 'is_error' : True, 'message' : 'Missing Parameters'}
    if(request.data.get('token')):
        try:
            tokenObject = CustomerToken.objects.get(key=request.data.get('token'))
            response['is_error'] = False
            tokenObject.delete()
            response['message'] = 'Successfully Logged Out'
            return JsonResponse(response)
        except CustomerToken.DoesNotExist:
            print response
            response['message'] = 'CustomerToken not found'
            return JsonResponse(response)
        except: 
            response['message'] = 'Internal Server Error. Try Again'
            return JsonResponse(response)
    else:
        return JsonResponse(response)


@api_view(['POST'])
def userLogin(request):
    response = { 'is_error' : True, 'message' : 'Mobile_no param not found','token':'','user_exists' : False}
    if(request.data.get('mobile_no')):
        mobileNo = request.data.get('mobile_no')
        if(not mobileNoStringValidator(mobileNo)):
            response['is_error'] = False
            response['message'] = 'Please Enter Valid Mobile No'
            return JsonResponse(response)  
        try:
            customer = Customer.objects.get(mobileNo=mobileNo)
            otpObject,state = OtpTable.objects.get_or_create(mobileNo=mobileNo)
            response['token'] = otpObject.token
            response['is_error'] = False
            response['user_exists'] = True
            response['message'] = 'An Otp has been sent to the mobile no '+mobileNo
            print response
            return JsonResponse(response)  
        except Customer.DoesNotExist:
            response['is_error'] = False
            response['message'] = 'User does not exist. Please Goto Signup Page'
            return JsonResponse(response)
        except: 
            response['message'] = 'Internal Server Error. Try Again'
            return JsonResponse(response)
    else:
        return JsonResponse(response)

@api_view(['POST'])
def userSignup(request):
    # Params {"mobile_no","referral_code"}
    response = { 'is_error' : True, 'user_exists' : True ,'message' : 'Missing Parameters','token':''}
    mobileNo = request.data.get('mobile_no')

    if (mobileNo):
        if(not mobileNoStringValidator(mobileNo)):
            response['message'] = 'Please Enter Correct Mobile No'
            return JsonResponse(response)
        try:
            customer = Customer.objects.get(mobileNo=mobileNo)
            response['is_error'] = False
            response['message'] = 'User already Exists. Please Goto Login Page'
            return JsonResponse(response)  
        except Customer.DoesNotExist:
            try:
                otpObject,state = OtpTable.objects.get_or_create(mobileNo=mobileNo)
                response['is_error'] = False
                response['token'] = otpObject.token
                response['user_exists'] = False
                response['message'] = 'An Otp has been sent to the mobile no '+mobileNo
                return JsonResponse(response)
            except: 
                response['message'] = 'Internal Server Error. Try Again'
                return JsonResponse(response)
        except: 
            response['message'] = 'Internal Server Error. Try Again'
            return JsonResponse(response)      
        
        response['message'] = 'Valid Params'
        return JsonResponse(response)   
    else:
        return Response(response)

@api_view(['POST'])
def makeBooking(request):
    # print request.data
    response = { 'is_error' : True, 'message' : 'Internal Server Error. Try Again'}
    customer_id = request.data.get("customer_id")
    partner_id = request.data.get("partner_id")
    service = request.data.get("data")
    serviceTime = request.data.get("time")
    total_amount = request.data.get("total_amount")
    try:
        bookingItem = Booking.objects.create(customer_id = customer_id,partner_id = partner_id,service = service,serviceTime = serviceTime,total_amount=total_amount)
        response['id'] = bookingItem.booking_id
        response['is_error'] = False
        response['message'] = "Booking Created Successfully"
        # print response
        return JsonResponse(response)
    except: 
        return JsonResponse(response)

@api_view(['POST'])
def getBookings(request):
    response = { 'is_error' : True, 'message' : 'Some Message','data': []}
    customer_id = request.data.get("customer_id")
    queryset = Booking.objects.filter(customer_id = customer_id)
    serializer = ShowBookingSerializer(queryset, many=True)
    response['data'] = serializer.data
    response['is_error'] = False
    response['message'] = 'Successful'
    # print response
    return Response(response)

@api_view(['POST'])
def getSingleBookings(request):
    response = { 'is_error' : True, 'message' : 'Some Message','data': {} }
    booking_id = request.data.get("booking_id")
    booking = Booking.objects.get(booking_id = booking_id)
    serializer = ShowBookingSerializer(booking)
    response['is_error'] = False
    response['message'] = 'Successful'
    response['data'] = serializer.data
    return Response(response) 

@api_view(['POST'])
def partnerLogin(request):
    print request.data
    response = { 'is_error' : True, 'message' : 'Please Enter a Valid Mobile No','success' : False}
    if(request.data.get('username') and request.data.get('password')):
        mobileNo = request.data.get('username')
        password = request.data.get('password')
        if(not mobileNoStringValidator(mobileNo)):
            response['is_error'] = False
            return JsonResponse(response)  
        try:
            partner = Partner.objects.get(mobileNo=mobileNo,password=password)
            partnerToken,state = PartnerToken.objects.get_or_create(partner=partner)
            response['token'] = partnerToken.key
            response['is_error'] = False
            response['data'] = PartnerSerializer(partner).data
            response['message'] = 'Successful'
            response['success'] = True
            return JsonResponse(response)
        except Partner.DoesNotExist:
            response['is_error'] = False
            response['message'] = 'Invalid User Credentials'
            return JsonResponse(response)
        except: 
            response['message'] = 'Internal Server Error. Try Again'
            return JsonResponse(response)
    else:
        return JsonResponse(response)


@api_view(['POST'])
def logoutPartner(request):
    response = { 'is_error' : True, 'message' : 'Missing Parameters'}
    if(request.data.get('token')):
        try:
            tokenObject = PartnerToken.objects.get(key=request.data.get('token'))
            response['is_error'] = False
            tokenObject.delete()
            response['message'] = 'Successfully Logged Out'
            return JsonResponse(response)
        except CustomerToken.DoesNotExist:
            print response
            response['message'] = 'CustomerToken not found'
            return JsonResponse(response)
        except: 
            response['message'] = 'Internal Server Error. Try Again'
            return JsonResponse(response)
    else:
        return JsonResponse(response)

@api_view(['POST'])
def partnerBooking(request):
    response = { 'is_error' : True, 'message' : 'Some Message','data': []}
    partner_id = request.data.get("partner_id")
    queryset = Booking.objects.filter(partner_id = partner_id)
    serializer = ShowBookingPartnerSerializer(queryset, many=True)
    response['data'] = serializer.data
    response['is_error'] = False
    response['message'] = 'Successful'
    # print response
    return Response(response)

@api_view(['POST'])
def partnerConfirmBooking(request):
    response = { 'is_error' : True, 'message' : 'Some Message','data': []}
    booking_id = request.data.get("booking_id")
    Booking.objects.filter(booking_id = booking_id).update(completed=True)
    response['is_error'] = False
    response['message'] = 'Successful'
    response['booking_id'] = booking_id
    # print response
    return Response(response)


@api_view(['POST'])
def partnerGetServices(request):
    response = { 'is_error' : True, 'message' : 'Some Message','data': []}
    partner_id = request.data.get("partner_id")
    serviceType = request.data.get("type")
    queryset = Service.objects.filter(partner_id = partner_id,serviceType = serviceType)
    serializer = ServicePartnerSerializer(queryset, many=True)
    response['data'] = serializer.data
    response['is_error'] = False
    response['message'] = 'Successful'
    # print response
    return Response(response)

@api_view(['POST'])
def partnerGetAllData(request):
    response = { 'is_error' : True, 'message' : 'Some message','data':{}}
    partner_id = request.data.get('partner_id')
    queryset0 = Booking.objects.filter(partner_id = partner_id)
    queryset1 = Service.objects.filter(partner_id=partner_id)
    queryset2 = ServiceDateTime.objects.filter(service_id__partner_id=partner_id)
    serializer0 = ShowBookingPartnerSerializer(queryset0, many=True)
    serializer1 = ServicePartnerSerializer(queryset1, many=True)
    serializer2 = ServiceDateTimePartnerSerializer(queryset2, many=True)
    response['data']['bookings'] = serializer0.data
    response['data']['services'] = serializer1.data
    response['data']['timings'] = serializer2.data
    response['is_error'] = False
    response['message'] = 'Successful'
    return Response(response)
