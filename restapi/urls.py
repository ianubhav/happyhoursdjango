from django.conf.urls import url, include
from rest_framework import routers
from restapi import views
from rest_framework import generics

# router = routers.DefaultRouter()
# router.register(r'partners', views.PartnerViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    # url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^userlogin/?$', views.userLogin),
    url(r'^usersignup/?$', views.userSignup),
    url(r'^verifyotp/?$', views.verifyOtp),
    url(r'^updateuser/?$', views.updateUser),
    url(r'^logoutuser/?$', views.logoutUser),
    url(r'^partnerlist/?$', views.PartnerList),
    url(r'^partnerhome/?$', views.PartnerHomePage),
    url(r'^getbookings/?$', views.getBookings),
    url(r'^makebooking/?$', views.makeBooking),
    url(r'^getsinglebooking/?$', views.getSingleBookings),


    url(r'^partnerlogin/?$', views.partnerLogin),
    url(r'^logoutpartner/?$', views.logoutPartner),
    url(r'^partnerbookings/?$', views.partnerBooking),
    url(r'^partner_confirm_booking/?$', views.partnerConfirmBooking),
    url(r'^partner_services/?$', views.partnerGetServices),
    url(r'^partner_alldata/?$', views.partnerGetAllData),
]

