from django.contrib import admin
from restapi.models import Customer,Partner,Service, OtpTable, Booking,ServiceDateTime,CustomerToken, PartnerToken
from django.contrib.auth.models import User

class OtpAdmin(admin.ModelAdmin):
    list_display = ('mobileNo', 'otp', 'created_at','expires_at','token')

class CustomerAdmin(admin.ModelAdmin):
	list_display = [f.name for f in Customer._meta.fields]

class PartnerAdmin(admin.ModelAdmin):
	list_display = [f.name for f in Partner._meta.fields]

class ServiceAdmin(admin.ModelAdmin):
	list_display = [f.name for f in Service._meta.fields] 	

class ServiceDateTimeAdmin(admin.ModelAdmin):
	list_display = [f.name for f in ServiceDateTime._meta.fields]

class CustomerTokenAdmin(admin.ModelAdmin):
	list_display = [f.name for f in CustomerToken._meta.fields] 

class PartnerTokenAdmin(admin.ModelAdmin):
	list_display = [f.name for f in PartnerToken._meta.fields] 	
admin.site.register(Customer, CustomerAdmin)
admin.site.register(Partner, PartnerAdmin)
admin.site.register(Service, ServiceAdmin)
admin.site.register(OtpTable, OtpAdmin)
admin.site.register(Booking)
admin.site.register(ServiceDateTime, ServiceDateTimeAdmin)
admin.site.register(CustomerToken, CustomerTokenAdmin)
admin.site.register(PartnerToken, PartnerTokenAdmin)
