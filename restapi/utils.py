from django.core.validators import RegexValidator
import re,random


regex='^[789]\d{9}$'
mobileNoValidator=[RegexValidator(regex=regex, message='Incorrect Mobile No', code='nomatch')]

def mobileNoStringValidator(mobileNo):
	return re.match(regex, mobileNo)

def generateOtp():
    # return 1234
    return random.randint(1111,9999)

def setTime(start,end):
			return start.strftime("%-I")+"-"+end.strftime("%-I%p")
		