from django.db import models
from restapi.utils import mobileNoValidator,generateOtp
from django.contrib.auth.models import User
from django.utils import timezone
from django.conf import settings
import random, uuid, shortuuid,string
from django.contrib.postgres.fields import JSONField
from geopy.distance import vincenty

def genBookingId():
    return ''.join(random.choice(string.uppercase + string.digits) for _ in range(8))
      
class Customer(models.Model):
    firstName = models.CharField(max_length=100,blank=True)
    lastName = models.CharField(max_length=100,blank=True)
    profilePicUrl = models.CharField(max_length=600,blank=True)
    mobileNo = models.CharField(max_length=10,unique = True,validators=mobileNoValidator)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    signupLat = models.DecimalField(max_digits=9, decimal_places=6,blank=True,null=True)
    signupLng = models.DecimalField(max_digits=9, decimal_places=6,blank=True,null=True)
    currentLat = models.DecimalField(max_digits=9, decimal_places=6,blank=True,null=True)
    currentLng = models.DecimalField(max_digits=9, decimal_places=6,blank=True,null=True)
    referralCode = models.CharField(max_length=10,blank=True)
    deviceId = models.CharField(max_length=100,blank=True)
    osType = models.CharField(max_length=10,blank=True)
    osVersion = models.CharField(max_length=10,blank=True)
    def __str__(self):
		return self.mobileNo



class Partner(models.Model):
    partnerName = models.CharField(max_length=100,blank=True)
    businessName = models.CharField(max_length=100,blank=True)
    tagLine = models.CharField(max_length=200,blank=True)
    city = models.CharField(max_length=100,blank=True)
    landlineNo = models.CharField(max_length=100,blank=True)
    partnerImgUrl = models.CharField(max_length=100,blank=True)
    mobileNo = models.CharField(max_length=10,unique = True,validators=mobileNoValidator)
    password = models.CharField(max_length=40)
    registrationNo = models.CharField(max_length=100,blank=True)
    panNo = models.CharField(max_length=100,blank=True)
    vatNo = models.CharField(max_length=100,blank=True)
    tinNo = models.CharField(max_length=100,blank=True)
    partnerTypes = (('party', 'Party'),('gym', 'Gym'),('salon', 'Salon'))
    serviceType = models.CharField(max_length=10,choices=partnerTypes)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    locationLat = models.DecimalField(max_digits=9, decimal_places=6,blank=True,null=True)
    locationLng = models.DecimalField(max_digits=9, decimal_places=6,blank=True,null=True)
    rating = models.IntegerField(blank=True,null=True)
    deviceId = models.CharField(max_length=100,blank=True)
    osType = models.CharField(max_length=10,blank=True)
    osVersion = models.CharField(max_length=10,blank=True)
    commisionPercentage = models.DecimalField( max_digits=5, decimal_places=2,blank=True,null=True)
    def distance(self,latitude,longitude):
        return vincenty((latitude,longitude),(self.locationLat,self.locationLng)).km
    def __str__(self):
        return self.businessName

class Service(models.Model):
    
    partner = models.ForeignKey('Partner',related_name='services')
    serviceName = models.CharField(max_length=100,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    fees = models.DecimalField(max_digits=10, decimal_places=2)
    discountFees = models.DecimalField(max_digits=10, decimal_places=2,editable = False)
    discountPercentage = models.IntegerField(default=0)
    serviceType = models.CharField(max_length=5, choices=( ('offer', 'Offer'),('other', 'Other') ) )
    runningStatus = models.BooleanField(default=True)
    # serviceImgUrl = models.CharField(max_length=600,blank=True)
    # maxNoAccept = models.IntegerField(blank=True,null=True)
    # rating = models.IntegerField(blank=True,null=True)
    def save(self, *args, **kwargs):
        self.discountFees = self.fees*(100-self.discountPercentage)/100
        super(Service, self).save(*args, **kwargs)
    def __str__(self):
        return self.serviceName        

class ServiceDateTime(models.Model):
    service = models.ForeignKey('Service')
    date = models.DateField()
    startTime = models.TimeField()
    endTime = models.TimeField()
    def __str__(self):
        return self.service.serviceName

class OtpTable(models.Model):
    mobileNo = models.CharField(max_length=10,validators=mobileNoValidator)
    otp = models.IntegerField(editable=False,null=True)
    token = models.CharField(max_length=30,editable=False)
    created_at = models.DateTimeField(editable=False)
    expires_at = models.DateTimeField(editable=False)

    def save(self, *args, **kwargs):
        timeNow = timezone.now()
        self.created_at = timeNow
        self.otp = 1234 #generateOtp()
        self.token = shortuuid.uuid()
        self.expires_at = timeNow + timezone.timedelta(minutes=settings.OTP_VALIDITY)
        super(OtpTable, self).save(*args, **kwargs)
    def __str__(self):
        return self.mobileNo

class Booking(models.Model):
    
    customer = models.ForeignKey('Customer')
    partner = models.ForeignKey('Partner')
    created_at = models.DateTimeField(auto_now_add=True)
    service = JSONField()
    serviceTime = models.CharField(max_length=20,blank=True)
    booking_id = models.CharField(max_length=20,editable=False)
    total_amount = models.IntegerField()
    completed = models.BooleanField(default=False)
    def save(self, *args, **kwargs):
        self.booking_id = genBookingId()
        super(Booking, self).save(*args, **kwargs)
    def __str__(self):
        return self.service 

class CustomerToken(models.Model):
    customer = models.ForeignKey('Customer')
    created_at = models.DateTimeField(auto_now_add=True)
    key = models.CharField(max_length=30,editable=False)
    def __str__(self):
        return self.customer.firstName
    
    def save(self, *args, **kwargs):
        self.key = shortuuid.uuid()
        super(CustomerToken, self).save(*args, **kwargs)

class PartnerToken(models.Model):
    partner = models.ForeignKey('Partner')
    created_at = models.DateTimeField(auto_now_add=True)
    key = models.CharField(max_length=30,editable=False)
    def __str__(self):
        return self.partner.businessName
    
    def save(self, *args, **kwargs):
        self.key = shortuuid.uuid()
        super(PartnerToken, self).save(*args, **kwargs)